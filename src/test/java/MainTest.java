import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void jsonToYaml() throws JsonProcessingException {
        String xmlContent ="{\"fsa:ResponseFsaType\":{\"xmlns:tns\":\"urn://x-artefact-rosaccreditation-ru/rdc/commons/1.0.2\"," +
                "\"xmlns:xsi\":\"http://www.w3.org/2001/XMLSchema-instance\",\"fsa:RdcTr\":{\"tns:Type\":\"TrRf\",\"tns:Requisites\":\"\"},\"" +
                "xmlns:fsa\":\"urn://x-artefact-rosaccreditation-ru/rdc/1.0.2\"}}";
        String actual = "---\n" +
                "fsa:ResponseFsaType:\n" +
                "  xmlns:tns: \"urn://x-artefact-rosaccreditation-ru/rdc/commons/1.0.2\"\n" +
                "  xmlns:xsi: \"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "  fsa:RdcTr:\n" +
                "    tns:Type: \"TrRf\"\n" +
                "    tns:Requisites: \"\"\n" +
                "  xmlns:fsa: \"urn://x-artefact-rosaccreditation-ru/rdc/1.0.2\"\n";

        String expected = Main.jsonToYaml(xmlContent);

        assertEquals(expected, actual);
    }
}